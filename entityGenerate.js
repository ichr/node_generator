const _ = require('lodash');

const PROPERTY_TYPES = {
    'character varying': 'string',
    'text': 'string',
    'uuid[]': 'string[]',
    'uuid': 'string',
    'integer': 'number',
    'smallint': 'number',
    'float': 'number',
    'boolean': 'boolean',
    'timestamp without time zone': 'Date'
};

const input = `   profile_ride_request_type page_profile_ride_request_type,
profile_ride_phone character varying(50) COLLATE pg_catalog."default",
profile_ride_phone_alt character varying(50) COLLATE pg_catalog."default",
profile_ride_first_name character varying(50) COLLATE pg_catalog."default",
profile_ride_last_name character varying(50) COLLATE pg_catalog."default",
profile_ride_email citext COLLATE pg_catalog."default",
profile_ride_num_passengers integer,
profile_ride_num_baggages integer,
profile_ride_ride_type page_profile_ride_ride_type,
profile_ride_features uuid[],
profile_ride_fleets uuid[],`;


const code = gen(input);

console.log(code);


function gen(input) {
    const parts = input.split('\n');
    const lines = parts.map(p => p.trim());
    let code = '';
    
    lines.forEach(line => {
        const columnName = getColumnName(line);
        const propertyName = _.camelCase(columnName);
        const propertyType = getPropertyType(line);

        code += `  @Column({name: '${columnName}'})
  ${propertyName}: ${propertyType};\n\n`;
    });

    return code;
}

function getColumnName(line) {
    return line.split(' ')[0];
}

function getPropertyType(line) {
    for(let key of Object.keys(PROPERTY_TYPES)) {
        if (line.indexOf(key) >= 0) {
            return PROPERTY_TYPES[key];
        }
    }

    return line.split(' ')[1];
}