const fs = require('fs');
const _ = require('lodash');

const name = 'page_profile_ride_ride_type';
const labels = 'regular, van, carpool, luxury';


const fileName = getFileName(name);
const code = gen(name, labels);

console.log(fileName, code);

createFile(fileName, code);


function gen(name, labels) {
    const enumName = genEnumName(name);
    const parts = labels.split(',');
    const values = parts.map(p => p.trim());
    const maxLength = values.reduce( (a, b) => { return a.length > b.length ? a : b; }).length;

    let code = `export enum ${enumName} {\n`;
    values.forEach(v => {
        const paddingSize = maxLength - v.length;
        
        const padding = ''.padStart(paddingSize);
        
        code += `  ${v} ${padding}   = '${v}',\n`;
    });

    code += '}\n';

    return code;
}

function getFileName(name) {
    return name.replace(/_/g, '-') + '.enum.ts';
}


function genEnumName(name) {
    let ret = _.camelCase(name);
    return ret.charAt(0).toUpperCase() + ret.slice(1);
}

function createFile(name, code) {
    fs.writeFileSync(name, code);
}